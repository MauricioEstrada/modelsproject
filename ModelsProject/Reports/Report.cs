﻿using ModelsProject.Models;
using ModelsProject.ORM;
using System.Collections.Generic;
using System.Linq;

namespace ModelsProject.Reports
{
    public class Report
    {
        private Model<Person> repositoryPerson;
        public Report()
        {
            DataGenerator data = new DataGenerator();
            this.repositoryPerson = data.RepositoryPerson;
        }

        public int CountDogs()
        {
            return (from a in this.repositoryPerson.GetAll()
                    where a.Dogs != null
                    select a.Dogs.Count()).Sum();
        }

        public Person OwnerWithTheMostDogs()
        {
            var query = this.repositoryPerson.GetAll().Where(l=>l.Dogs != null)
                   .OrderByDescending(t => t.Dogs.Count())
                   .FirstOrDefault();
            return query;
        }

        public Breed MostCommonBreed()
        {
            var listBreed = new List<Breed>();
            foreach(var item in this.repositoryPerson.GetAll().Where(l=>l.Dogs!=null))
            {
                foreach (var item2 in item.Dogs)
                {
                    listBreed.Add(item2.Breed);
                }
            }

            var result = (from p in listBreed
                         group p by p.Id into g
                         select new { IdBreed = g.Key, NameBreed = g.FirstOrDefault().Name, Count = g.Count() })
                         .OrderByDescending(l=>l.Count)
                         .FirstOrDefault();

            return new Breed { Id = result.IdBreed, Name = result.NameBreed };
        }

        public Person OwnerWithTheMostBreeds()
        {
            var listPersonBreed = new List<dynamic>();
            foreach (var item in this.repositoryPerson.GetAll().Where(l => l.Dogs != null))
            {
                foreach (var item2 in item.Dogs)
                {
                    listPersonBreed.Add(new { Person = item, Breed = item2.Breed});
                }
            }

            var result = (from p in listPersonBreed
                          group p by p.Person.Id into g
                          select new { IdPerson = g.Key, NamePerson = g.FirstOrDefault().Person.Name, PersonDogs = g.FirstOrDefault().Person.Dogs, Count = g.Select(b => b.Breed.Name).Distinct().Count() })
                        .OrderByDescending(l => l.Count)
                        .FirstOrDefault();

            return new Person { Id = result.IdPerson, Name = result.NamePerson , Dogs = result.PersonDogs};
        }
    }
}
