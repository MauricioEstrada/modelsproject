﻿using ModelsProject.ORM;
using System.Collections.Generic;

namespace ModelsProject.Models
{
    public class Person : Model<Person>, IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Dog> Dogs { get; set; }

        public override string ToString()
        {
            return "Person: " + Name + ", Id " + Id;
        }

    }
}
