﻿using ModelsProject.ORM;

namespace ModelsProject.Models
{
    public class Breed : Model<Breed>, IEntity 
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return "Breed: " + Name + ", Id " + Id;
        }
    }
}
