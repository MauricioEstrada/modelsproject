﻿using ModelsProject.ORM;

namespace ModelsProject.Models
{
    public class Dog : Model<Dog>, IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Breed Breed { get; set; }

        public override string ToString()
        {
            return "Dog: " + Name + ", Id " + Id;
        }
    }
}
