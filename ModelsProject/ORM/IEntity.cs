﻿namespace ModelsProject.ORM
{
    public interface IEntity
    {
        int Id { get; }
    }
}
