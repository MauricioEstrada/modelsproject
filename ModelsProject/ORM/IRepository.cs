﻿using System.Collections.Generic;

namespace ModelsProject.ORM
{
    public interface IRepository<T>
    {
        void Create(T entity); 

        T Read(int id);

        bool Update(T entity);

        bool Delete(T entity);

        List<T> GetAll();
    }
}
