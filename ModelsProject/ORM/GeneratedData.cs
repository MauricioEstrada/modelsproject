﻿using ModelsProject.Models;
using System.Collections.Generic;

namespace ModelsProject.ORM
{
    public class DataGenerator
    {
        private Breed repoBreed;
        private Dog repoDog;
        private Person repoPerson;

        public DataGenerator()
        {
            #region Repository Breed
            
            this.repoBreed = new Breed();

            this.repoBreed.Create(new Breed
            {
                Id = 1,
                Name = "Beagle"
            });
            this.repoBreed.Create(new Breed
            {
                Id = 2,
                Name = "Bulldog"
            });
            this.repoBreed.Create(new Breed
            {
                Id = 3,
                Name = "Caniche"
            });
            this.repoBreed.Create(new Breed
            {
                Id = 4,
                Name = "Dóberman"
            });
            this.repoBreed.Create(new Breed
            {
                Id = 5,
                Name = "Bóxer"
            });

            #endregion

            #region Repository Dogs
            
            this.repoDog = new Dog();

            // Dogs Person 100, 2 breed, 1, 1, 1, 5, 5, 5
            this.repoDog.Create(new Dog
            {
                Id = 1001,
                Name = "Perro 1001",
                Breed = repoBreed.Read(1)
            });
            this.repoDog.Create(new Dog
                {
                    Id = 1002,
                    Name = "Perro 1002",
                    Breed = repoBreed.Read(1)
                });
            this.repoDog.Create(new Dog
                {
                    Id = 1003,
                    Name = "Perro 1003",
                    Breed = repoBreed.Read(5)
                });
            this.repoDog.Create(new Dog
                {
                    Id = 1004,
                    Name = "Perro 1004",
                    Breed = repoBreed.Read(5)
                });
            this.repoDog.Create(new Dog
                {
                    Id = 1005,
                    Name = "Perro 1004",
                    Breed = repoBreed.Read(5)
                });
            this.repoDog.Create(new Dog
                {
                    Id = 1006,
                    Name = "Perro 1004",
                    Breed = repoBreed.Read(5)
                });

            // Dogs Person 200, 3 breed, 1, 2, 5, 5
            this.repoDog.Create(new Dog
                {
                    Id = 2001,
                    Name = "Perro 2001",
                    Breed = repoBreed.Read(1)
                });
            this.repoDog.Create(new Dog
                {
                    Id = 2002,
                    Name = "Perro 2002",
                    Breed = repoBreed.Read(2)
                });
            this.repoDog.Create(new Dog
                {
                    Id = 2003,
                    Name = "Perro 2003",
                    Breed = repoBreed.Read(5)
                });
            this.repoDog.Create(new Dog
                {
                    Id = 2004,
                    Name = "Perro 2004",
                    Breed = repoBreed.Read(5)
                });

            // Dogs Person 300, 2 breed, 1, 2, 3, 4, 5
            this.repoDog.Create(new Dog
                {
                    Id = 3001,
                    Name = "Perro 3001",
                    Breed = repoBreed.Read(3)
                });
            this.repoDog.Create(new Dog
                {
                    Id = 3002,
                    Name = "Perro 3002",
                    Breed = repoBreed.Read(2)
                });
            this.repoDog.Create(new Dog
                {
                    Id = 3003,
                    Name = "Perro 3003",
                    Breed = repoBreed.Read(1)
                });
            this.repoDog.Create(new Dog
                {
                    Id = 3004,
                    Name = "Perro 3004",
                    Breed = repoBreed.Read(4)
                });
            this.repoDog.Create(new Dog
                {
                    Id = 3005,
                    Name = "Perro 3005",
                    Breed = repoBreed.Read(5)
                });

            // Dogs Person 400, 1 breed, 2
            this.repoDog.Create(new Dog
                {
                    Id = 4001,
                    Name = "Perro 4001",
                    Breed = repoBreed.Read(2)
                });

            #endregion

            #region Repository Person
            
            this.repoPerson = new Person();

            this.repoPerson.Create(new Person
                {
                    Id = 100,
                    Name = "José",
                    Dogs = new List<Dog> { repoDog.Read(1001), repoDog.Read(1002), repoDog.Read(1003), repoDog.Read(1004), repoDog.Read(1005), repoDog.Read(1006) }
                });
            this.repoPerson.Create(new Person
                {
                    Id = 200,
                    Name = "Rodrigo",
                    Dogs = new List<Dog> { repoDog.Read(2001), repoDog.Read(2002), repoDog.Read(2003), repoDog.Read(2004) }
                });
            this.repoPerson.Create(new Person
                {
                    Id = 300,
                    Name = "Juan",
                    Dogs = new List<Dog> { repoDog.Read(3001), repoDog.Read(3002), repoDog.Read(3003), repoDog.Read(3004), repoDog.Read(3005) }
                });
            this.repoPerson.Create(new Person
                {
                    Id = 400,
                    Name = "Felipe",
                    Dogs = new List<Dog> { repoDog.Read(4001) }
                });
            this.repoPerson.Create(new Person
                {
                    Id = 500,
                    Name = "Gastón",
                    Dogs = null
                });

            #endregion
        }

        public Model<Person> RepositoryPerson
        {
            get => this.repoPerson;
        }
    }
}
