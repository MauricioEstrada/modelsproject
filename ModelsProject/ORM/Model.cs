﻿using System.Collections.Generic;
using System.Linq;

namespace ModelsProject.ORM
{
    public class Model<T> : IRepository<T> where T : class, IEntity
    {
        private List<T> _list;
        public Model()
        {
            this._list = new List<T>();            
        }

        public void Create(T entity)
        {
            this._list.Add(entity);
        }

        public T Read(int id) 
        {
            return this._list.FirstOrDefault(t=>t.Id==id);
        }

        public bool Update(T entity)
        {
            var index = this._list.FindIndex(t => t.Id == entity.Id);

            if (index > 0)
            {
                _list[index] = entity;
                return true;
            }

            return false;
        }

        public bool Delete(T entity)
        {
            var aux = this._list.Single(t => t.Id == entity.Id);

            if (aux != null)
            {
                return _list.Remove(aux);
            }

            return false;
        }

        public List<T> GetAll()
        {
            return this._list;
        }
    }

}
