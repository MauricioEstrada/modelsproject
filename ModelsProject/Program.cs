﻿using ModelsProject.Reports;
using System;

namespace ModelsProject
{
    class Program
    {
        static void Main(string[] args)
        {
            // Initialize Report object
            var report = new Report();

            // Print lines in the console
            Console.WriteLine("CountDogs : " + report.CountDogs());
            Console.WriteLine("OwnerWithTheMostDogs : " + report.OwnerWithTheMostDogs());
            Console.WriteLine("MostCommonBreed : " + report.MostCommonBreed());
            Console.WriteLine("OwnerWithTheMostBreeds : " + report.OwnerWithTheMostBreeds());
            
            Console.ReadLine();
        }
    }
}
